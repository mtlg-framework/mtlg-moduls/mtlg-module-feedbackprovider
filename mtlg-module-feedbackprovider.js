var init = function(options) {
  console.log('feedbackprovider_init', options);
  if (options) {
    _baseUrl = options.FEEDBACKPROVIDER_BASE_URL || _baseUrl;
    game_id = options.feedbackproviderGameUuid || game_id;
    student_id = MTLG.getPlayer(0) || student_id;
  }
  console.log(MTLG.getPlayerNumber());
  if (MTLG.utils.fbm === undefined) {
    return
  }

  old_feedback = MTLG.utils.fbm.giveFeedback;

  console.log('feedbackprovider module loaded.');

  // Wenn getPlayer() implementiert, muss preload ausgeführt werden nachdem der Login oä stattgefunden hat.
  //
  preload_feedback();
  patch_fbm_methood()
};

var _baseUrl = 'http://localhost:8000/';

var feedbackprovider_cache = {};
var game_id = "4ab4955b-c36c-48c3-9b84-9f329905484c";
var student_id = "efb91e6c-732a-436c-bc17-512ac38a127a";

let old_feedback = null;
let server_res = null

async function preload_feedback() {
    for(let i = 0; i < MTLG.getPlayerNumber(); i++) {
      let student_id = MTLG.getPlayer(i).uuid;
      let url = `${_baseUrl}api/v1/preload_feedback/${game_id}/student/${student_id}`;
      try {
          server_res = await fetch(url)
              .then(function (response) {
                  return response.json();
              })
      }
      catch(e) {
          console.log(`Failed to get feedback for user ${student_id}`);
          console.log(e)
          return
      }
      if (server_res && server_res.payload) {
          feedbackprovider_cache[student_id] = server_res.payload.r;
          // Fill fbm with feedback that is only defined in DB and not code
          for (const [content, data] of Object.entries(feedbackprovider_cache[student_id])) {
            if(MTLG.utils.fbm.getContent(content).name === undefined) {
              var contentFBM = MTLG.utils.fbm.createContent(content);
              Object.entries(data.attributes).forEach((item, i) => {
                contentFBM[i] = item;
              });
            }
          }

      }
      else {
          console.log("Got invalid response from Feedbackprovider-Server")
      }
    }

}

/*
 * Returns information about this module
 * @memberof MTLG.feedbackprovider#
 *
 * @return {!object} Object with name and note attributes
 */
function info() {
  return {
    name: 'feedbackprovider modul',
    note: 'MTLG-Modul that allows to personalize feedback'
  }
}

async function new_giveFeedback(target, content, timing, style, player = null) {

    let additional_attrs = null;
    if(player && !feedbackprovider_cache[player.uuid]) {
      await preload_feedback()
    }
    if (player && player.uuid && feedbackprovider_cache[player.uuid] && content in feedbackprovider_cache[player.uuid]) {
        additional_attrs = feedbackprovider_cache[player.uuid][content].attributes;
    }
    return old_feedback(target, content, timing, style, additional_attrs)
}

function patch_fbm_methood()  {
    MTLG.utils.fbm.giveFeedback = new_giveFeedback;
    console.log("Patched fbm.giveFeedback-Method")
}

// injecting into MTLG
MTLG.feedbackprovider = {
  init: init,
  info: info
};
MTLG.addModule(init, info);
